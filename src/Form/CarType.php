<?php

namespace App\Form;

use App\Entity\Car;
use App\Entity\CarCategory;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class, array(
                'attr' => array('class' => 'form-control','placeholder' => 'Car name '),
                'label'=>false,
                'required' => false))
            ->add('cost',IntegerType::class,array(
                'attr' => array('class' => 'form-control','placeholder' => 'cost'),
                'label'=>false,
                'required' => false))
            ->add('nbDoors',TextType::class,array(
                'attr' => array('class' => 'form-control','placeholder' => 'number Doors'),
                'label'=>false,
                'required' => false))
            ->add('nbSeats',TextType::class,array(
                'attr' => array('class' => 'form-control','placeholder' => 'number Seats'),
                'label'=>false,
                'required' => false))
            ->add('carCategory', EntityType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choice_label'=> 'name',
                    'required' => true,
                    'label'=>false,
                    'class' => CarCategory::class,
                    'expanded' => false,
                    'multiple' => false,)
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
