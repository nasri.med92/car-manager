<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarType;
use App\Service\TemperatureService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AbstractController
{
    public $em;
    public $paginator;


    public function __construct(EntityManagerInterface $em,PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/car", name="app_car_index", methods={"GET","POST"})
     */
    public function index(Request $request,TemperatureService $temperatureService)
    {
        // On récupère les filtres
        $data = $request->request->all();
        $filters = [
            'car_name' => $data['car_name'] ?? null,
            'car_category' => $data['car_category'] ?? null,
        ];
        if ($request->isMethod('post')) {
            $cars = $this->em->getRepository(Car::class)->getCarByNameCategory($filters);
        } else {
            $cars = $this->em->getRepository(Car::class)->findAll();
        }
        $latitude = 52.52;
        $longitude = 13.41;
        $dateTime = new \DateTime();
        $dateNow = $dateTime->format('Y-m-d\TH').":00";
        $temperaturesData = $temperatureService->getHourlyTemperature($latitude, $longitude);
        $index = 0 ;
        $temperature = 0 ;
        foreach ($temperaturesData as $key => $value) {
            if ($key == 'time') {
                foreach ($value as $k=> $time) {
                    if ($time == $dateNow) {
                        $index=$k;
                        break;
                    }
                }
            }
            if ($key == 'temperature_2m') {
                foreach ($value as $k1 => $value) {
                    if ($index == $k1) {
                        $temperature = $value;
                    }
                }
            }

        }
        $data = $this->paginator->paginate(
            $cars,
            $request->query->getInt('page', 1), 20
        );
        return $this->render('car/index.html.twig',[
            'cars'=>$data,'date_now'=>$dateTime,'temperature'=>$temperature
        ]);
    }

    /**
     * Creates a new Car entity.
     *
     * @Route("/new", name="car_new", methods={"GET", "POST"})
     */
    public function new(Request $request)
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($car);
            $this->em->flush();

            return $this->redirectToRoute('app_car_index');
        }

        return $this->render('car/new.html.twig', array(
            'car' => $car,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="car_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id): Response
    {
        $car = $this->em->getRepository(Car::class)->find($id);
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('app_car_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('car/edit.html.twig', [
            'car' => $car,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="car_delete", methods={"POST"})
     */
    public function delete(Request $request,$id): Response
    {
        $car = $this->em->getRepository(Car::class)->find($id);
        if ($this->isCsrfTokenValid('delete'.$car->getId(), $request->request->get('_token'))) {
            $this->em->remove($car);
            $this->em->flush();
        }

        return $this->redirectToRoute('app_car_index', [], Response::HTTP_SEE_OTHER);
    }


}
