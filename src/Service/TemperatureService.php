<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class TemperatureService
{
    function getHourlyTemperature($latitude, $longitude)
    {
        $httpClient = HttpClient::create();

        $url = 'https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m';

        $response = $httpClient->request('GET', $url);

        if ($response->getStatusCode() === 200) {
            $content = $response->getContent();
            $data = json_decode($content, true);

            // Access the hourly temperature data
            $hourlyTemperature = $data['hourly'];

            return $hourlyTemperature;
        } else {
            return null;
        }
    }

}