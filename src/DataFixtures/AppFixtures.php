<?php

namespace App\DataFixtures;

use App\Entity\Car;
use App\Entity\CarCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $categories = [
            [
                'name' => 'category A'
            ],
            [
                'name'=> 'category B',
            ],
            [
                'name' => 'category C'
            ],
            [
                'name'=> 'category D',
            ],
            [
                'name' => 'category E'
            ],
        ];
        $cars = [
            [
                'name' => 'BMW'
            ],
            [
                'name'=> 'nissan',
            ],
            [
                'name' => 'peugeot'
            ],
            [
                'name'=> 'citroen',
            ],
            [
                'name' => 'mercedes'
            ],
            [
                'name' => 'Toyota'
            ],
            [
                'name'=> 'Honda',
            ],
            [
                'name' => 'Chevrolet '
            ],
            [
                'name'=> 'Ford',
            ],
            [
                'name' => 'Audi'
            ],
            [
                'name' => 'Volkswagen'
            ],
            [
                'name'=> 'Hyundai ',
            ],
            [
                'name' => 'Mazda'
            ],
            [
                'name'=> 'Kia',
            ],
            [
                'name' => 'Lexus'
            ],
            [
                'name' => 'Tesla'
            ],
            [
                'name'=> 'Volvo',
            ],
            [
                'name' => 'Jaguar'
            ],
            [
                'name'=> 'Jeep',
            ],
            [
                'name' => 'Porsche'
            ],
            [
                'name' => 'Subaru Impreza'
            ]
        ];

        foreach ($categories as $item) {
            $category = new CarCategory();
            $category->setName($item['name']);
            $manager->persist($category);

            $categoryReference[$item['name']] = $category;
        }

        foreach ($cars as $item) {
            $car = new Car();
            $car->setName($item['name'])
                ->setCost($this->faker->randomFloat())
                ->setNbDoors($this->faker->numberBetween(2,6))
                ->setNbSeats($this->faker->numberBetween(2,6));

            $randomCategoryName = $categories[array_rand($categories)]['name'];
            $randomCategory = $categoryReference[$randomCategoryName];

            $car->setCarCategory($randomCategory);
            $manager->persist($car);
        }

        $manager->flush();
    }
}
